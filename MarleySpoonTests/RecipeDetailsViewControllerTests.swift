//
//  RecipeDetailsViewController.swift
//  MarleySpoonTests
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation
import XCTest
@testable import MarleySpoon

final class RecipeDetailsViewControllerTests: XCTestCase {
    var recipeDetailsViewController: RecipeDetailsViewController!

    override func setUp() {
        super.setUp()

        let  recipeDetailsModel = UIRecipeDetailsModel(title: "White Cheddar Grilled Cheese with Cherry Preserves & Basil", imagePath: URL(string: "https://images.ctfassets.net/kk2bw5ojx476/61XHcqOBFYAYCGsKugoMYK/0009ec560684b37f7f7abadd66680179/SKU1240_hero-374f8cece3c71f5fcdc939039e00fb96.jpg")!, description: "*Grilled Cheese 101*", chefName: "Ahmed", tags: "vegan")

        recipeDetailsViewController =  RecipeDetailsViewController(recipeDetailsModel)

        // Arrange: setup UINavigationController
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.rootViewController = UINavigationController(rootViewController: recipeDetailsViewController)
    }

    override func tearDown() {
        recipeDetailsViewController = nil

    }

    func test_views_with_fullData() {
        let  recipeDetailsModel = UIRecipeDetailsModel(title: "White Cheddar Grilled Cheese with Cherry Preserves & Basil", imagePath: URL(string: "https://images.ctfassets.net/kk2bw5ojx476/61XHcqOBFYAYCGsKugoMYK/0009ec560684b37f7f7abadd66680179/SKU1240_hero-374f8cece3c71f5fcdc939039e00fb96.jpg")!, description: "*Grilled Cheese 101*", chefName: "Ahmed", tags: "vegan")

        recipeDetailsViewController =  RecipeDetailsViewController(recipeDetailsModel)

        (1...8).forEach { tag in
            let label = recipeDetailsViewController.view.viewWithTag(tag) as? UILabel
            XCTAssertNotNil(label)
            if tag == 2 {
                XCTAssertEqual(label?.text, "White Cheddar Grilled Cheese with Cherry Preserves & Basil")
            }

            if tag == 4 {
                XCTAssertEqual(label?.text, "Ahmed")
            }

            if tag == 6 {
                XCTAssertEqual(label?.text, "*Grilled Cheese 101*")
            }

            if tag == 8 {
                XCTAssertEqual(label?.text, "vegan")
            }

        }
    }

    func test_views_without_tag() {
        let  recipeDetailsModel = UIRecipeDetailsModel(title: "White Cheddar Grilled Cheese with Cherry Preserves & Basil", imagePath: URL(string: "https://images.ctfassets.net/kk2bw5ojx476/61XHcqOBFYAYCGsKugoMYK/0009ec560684b37f7f7abadd66680179/SKU1240_hero-374f8cece3c71f5fcdc939039e00fb96.jpg")!, description: "*Grilled Cheese 101*", chefName: "Ahmed", tags: "")

        recipeDetailsViewController =  RecipeDetailsViewController(recipeDetailsModel)

        (1...8).forEach { tag in
            let label = recipeDetailsViewController.view.viewWithTag(tag) as? UILabel
            if tag == 2 {
                XCTAssertNotNil(label)
                XCTAssertEqual(label?.text, "White Cheddar Grilled Cheese with Cherry Preserves & Basil")
            }

            if tag == 4 {
                XCTAssertNotNil(label)
                XCTAssertEqual(label?.text, "Ahmed")
            }

            if tag == 6 {
                XCTAssertNotNil(label)
                XCTAssertEqual(label?.text, "*Grilled Cheese 101*")
            }

            if tag == 8 {
                XCTAssertNil(label)
            }

        }
    }

}
