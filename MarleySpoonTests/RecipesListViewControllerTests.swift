//
//  RecipesListViewControllerTests.swift
//  MarleySpoonTests
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation
import XCTest
@testable import MarleySpoon

final class RecipesListViewControllerTests: XCTestCase {

    var recipesListViewController: RecipesListViewController!

    override func setUp() {
        super.setUp()
        recipesListViewController =  RecipesListBuilder.viewController()

        // Arrange: setup UINavigationController
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        keyWindow?.rootViewController = UINavigationController(rootViewController: recipesListViewController)
    }

    override func tearDown() {
        recipesListViewController = nil

    }

    func test_getRecipes_success() {
        let expectation = XCTestExpectation()

        let mockAPIClient = MockAPIClient.createMockSession(fromJsonFile: "data", andError: nil)
        let repository = getMockWebRecipesRepository(mockAPIClient: mockAPIClient)
        let router = RecipesListRouter(viewController: recipesListViewController)
        let presenter = RecipesListPresenter(output: recipesListViewController, router: router, recipesRepository: repository)
        recipesListViewController.presenter = presenter

        // fire getRecipes after load viewController
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            presenter.getRecipes()
        }

        // Check the datasource after get recipes bind to CollectionView
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertNotNil(self.recipesListViewController.collectionDataSource)
            XCTAssertNotNil(self.recipesListViewController.collectionDataSource?.presenterInput)
            XCTAssertEqual(self.recipesListViewController.collectionDataSource?.itemsForCollection.count, 4)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 3)
    }

    func getMockWebRecipesRepository(mockAPIClient: MockAPIClient) -> WebRecipesRepository {
        return WebRecipesRepository(client: mockAPIClient)
    }
}
