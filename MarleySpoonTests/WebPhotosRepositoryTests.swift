//
//  WebRecipesRepositoryTests.swift
//  MarleySpoonTests
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import XCTest
@testable import MarleySpoon

final class WebRecipesRepositoryTests: XCTestCase {
    var webRecipesRepository: WebRecipesRepository!

    override func setUp() {
        // Arrange: setup ViewModel
        webRecipesRepository = WebRecipesRepository()
    }

    override func tearDown() {
        webRecipesRepository = nil
    }

    func test_GetItems_FromAPI() {
        let expectation = XCTestExpectation()
        let mockAPIClient = MockAPIClient.createMockSession(fromJsonFile: "data", andError: nil)
        webRecipesRepository = WebRecipesRepository(client: mockAPIClient)
        // Act: get data from API .
        webRecipesRepository.recipes { (result) in
            switch result {
            case .success(let recipes):
                guard !recipes.isEmpty else {
                    return
                }
                // Assert: Verify it's have a data.
                XCTAssertGreaterThan(recipes.count, 0)
                XCTAssertEqual(recipes.count, 4)
                expectation.fulfill()
            default:
                XCTFail("Can't get Data")
            }

        }
        wait(for: [expectation], timeout: 2)
    }

    func test_NoResult_FromAPI() {
        let expectation = XCTestExpectation()
        
        let mockAPIClient = MockAPIClient.createMockSession(fromJsonFile: "noData", andError: nil)
        webRecipesRepository = WebRecipesRepository(client: mockAPIClient)
        // Act: get data from API .
        webRecipesRepository.recipes { (result) in
            switch result {
            case .success(let recipes):
                // Assert: Verify it's have a data.
                XCTAssertEqual(recipes.count, 0)
                expectation.fulfill()
            default:
                XCTFail("Can't get Data")
            }

        }
        wait(for: [expectation], timeout: 2)
    }

}
