//
//  NetworkMock.swift
//  MarleySpoonTests
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import UIKit
import Contentful
@testable import MarleySpoon

final class MockAPIClient: URLSessionProtocol {

    private var completionHandler: (Data?, MarleySpoonError?)

    private let client: Client
    private(set) var jsonDecoder: JSONDecoder?

    init(completionHandler: (Data?, MarleySpoonError?)) {
        let spaceId = "kk2bw5ojx476"
        let accessToken = "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c"
        client = Client(spaceId: spaceId,
                        environmentId: "master",
                        accessToken: accessToken,
                        contentTypeClasses: [Recipe.self, Chef.self, Tag.self])
        self.completionHandler = completionHandler
        initJSONDecoder()

    }

    func loadData<T>(completion: @escaping (Result<[T], MarleySpoonError>) -> Void)  where T: FieldKeysQueryable & EntryDecodable {

        if let error = completionHandler.1 {
            completion(.failure(error))
            return
        }

        guard let data = completionHandler.0 else {
            completion(.failure(.noResults))
            return
        }

        do {
            let decodedObject = try jsonDecoder?.decode(HomogeneousArrayResponse<T>.self, from: data)
            completion(.success(decodedObject?.items ?? []))
        } catch {
            completion(.failure(.parseError))
        }
    }

    private func initJSONDecoder() {
        guard jsonDecoder == nil else {
            return
        }
        jsonDecoder = client.jsonDecoder
        let data = DataLoader().loadJsonData(file: "locals")
        let decodedObject = try! client.jsonDecoder.decode(HomogeneousArrayResponse<Contentful.Locale>.self, from: data!)
        if let localizationContext = LocalizationContext(locales: decodedObject.items ) {
            self.jsonDecoder?.update(with: localizationContext)
        }
    }

    static func createMockSession(fromJsonFile file: String,
                                  andError error: MarleySpoonError?) -> MockAPIClient {

        let data = DataLoader().loadJsonData(file: file)

        return MockAPIClient(completionHandler: (data, error))
    }
}
