//
//  RecipesListPresenterTests.swift
//  MarleySpoonTests
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import XCTest
@testable import MarleySpoon

final class RecipesListPresenterTests: XCTestCase {
    var mockRecipesListPresenterOutput: MockRecipesListPresenterOutput!

    override func setUp() {
        mockRecipesListPresenterOutput = MockRecipesListPresenterOutput()
    }

    override func tearDown() {
        mockRecipesListPresenterOutput = nil
        Reachability.shared =  MockReachability(internetConnectionState: .satisfied)
    }

    func test_getRecipes_success() {
        let presenter = getRecipesListPresenter(fromJsonFile: "data")
        presenter.getRecipes()
        XCTAssertEqual(mockRecipesListPresenterOutput.itemsForCollection.count, 4)
    }

    func test_getRecipes_noResult() {
        let presenter = getRecipesListPresenter(fromJsonFile: "noData")
        presenter.getRecipes()
        XCTAssertEqual(mockRecipesListPresenterOutput.itemsForCollection.count, 0)
        if let error = mockRecipesListPresenterOutput.error as? MarleySpoonError {
            switch error {
            case .noResults:
                XCTAssertTrue(true)
            default:
                XCTFail("the error isn't noResults")
            }
        }
    }

    func test_getRecipes_noInternetConnection() {
        Reachability.shared =  MockReachability(internetConnectionState: .unsatisfied)
        let presenter = getRecipesListPresenter(fromJsonFile: "noData")
        presenter.getRecipes()
        XCTAssertEqual(mockRecipesListPresenterOutput.itemsForCollection.count, 0)
        if let error = mockRecipesListPresenterOutput.error as? MarleySpoonError {
            switch error {
            case .noInternetConnection:
                XCTAssertTrue(true)
            default:
                XCTFail("the error isn't noResults")
            }
        }
    }

    private func getMockWebRecipesRepository(mockAPIClient: MockAPIClient) -> WebRecipesRepository {
        return WebRecipesRepository(client: mockAPIClient)
    }

    private func getRecipesListPresenter(fromJsonFile file: String) -> RecipesListPresenter {
        let mockAPIClient = MockAPIClient.createMockSession(fromJsonFile: file, andError: nil)
        let repository = getMockWebRecipesRepository(mockAPIClient: mockAPIClient)
        let router = RecipesListRouter(viewController: mockRecipesListPresenterOutput)
        return RecipesListPresenter(output: mockRecipesListPresenterOutput, router: router, recipesRepository: repository)
    }
}

final class MockRecipesListPresenterOutput: UIViewController, RecipesListPresenterOutput {

    func emptyState(emptyPlaceHolderType: EmptyPlaceHolderType) {

    }

    var itemsForCollection: [ItemCollectionViewCellType] = []
    var error: Error!

    func updateData(error: Error) {
        self.error = error
    }

    func updateData(itemsForCollection: [ItemCollectionViewCellType]) {
        self.itemsForCollection = itemsForCollection
    }
}
