//
//  Strings.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation

enum Strings: String {

    // MARK: Errors
    case commonGeneralError = "Common_GeneralError"
    case commonInternetError = "Common_InternetError"

    // MARK: MarleySpoon
    case marleySpoonTitle = "MarleySpoon_Title"

    // MARK: Internet Errors
    case noInternetConnectionTitle = "No_Internet_Connection_Title"
    case noInternetConnectionSubtitle = "No_Internet_Connection_Subtitle"

    // MARK: Recipes Errors
    case noRecipesErrorTitle = "No_Recipes_Error_Title"
    case noRecipesErrorSubtitle = "No_Recipes_Error_Subtitle"

    case tryAction = "Try_Action"

    func localized() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
