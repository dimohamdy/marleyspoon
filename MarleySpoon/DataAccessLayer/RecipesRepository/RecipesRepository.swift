//
//  RecipesRepository.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation

protocol RecipesRepository {

    func recipes(completion: @escaping (Result< [Recipe], MarleySpoonError>) -> Void)
}
