//
//  WebRecipesRepository.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation

final class WebRecipesRepository {
    
    let client: URLSessionProtocol
    init(client: URLSessionProtocol = APIClient()) {
        self.client =  client
    }
    
    func recipes(completion: @escaping (Result< [Recipe], MarleySpoonError>) -> Void) {
        client.loadData(completion: { (result: Result<[Recipe], MarleySpoonError>) in
            switch result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
