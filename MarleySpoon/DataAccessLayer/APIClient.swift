//
//  APIClient.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation
import Contentful

// MARK: - URLSession
protocol URLSessionProtocol {
    func loadData<T>(completion: @escaping (Result<[T], MarleySpoonError>) -> Void) where T: FieldKeysQueryable & EntryDecodable & Resource
}

final class APIClient: URLSessionProtocol {
    
    private let spaceId: String
    private let accessToken: String
    let client: Client
    
    init() {
        // in real application I use cocoapods-keys to secure the keys
        self.spaceId = "kk2bw5ojx476"
        self.accessToken = "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c"
        client = Client(spaceId: spaceId,
                        environmentId: "master",
                        accessToken: accessToken,
                        contentTypeClasses: [Recipe.self, Chef.self, Tag.self])
        
        #if DEBUG
        ContentfulLogger.logLevel = .info
        #endif
    }
    
    func loadData<T>(completion: @escaping (Result<[T], MarleySpoonError>) -> Void) where T: FieldKeysQueryable & EntryDecodable & Resource {
        client.fetchArray(of: T.self) { result in
            switch result {
            case .success(let response):
                completion(.success(response.items))
                
            case .failure(let error):
                completion(.failure(error as? MarleySpoonError ?? .parseError))
            }
        }
    }
}
