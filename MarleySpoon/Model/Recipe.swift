//
//  Recipe.swift
//  MarleySpoon
//
//  Created by Christian on 30/05/19.
//  Copyright © 2019 Christian. All rights reserved.
//

import Foundation
import Contentful

class Recipe: EntryDecodable, Resource, FieldKeysQueryable {

    static let contentTypeId = "recipe"
    
    let sys: Sys
    let title: String
    var photo: Asset?
    let description: String
    var chefName: String?
    var tags: [String]?
    
    required init(from decoder: Decoder) throws {
        
        sys          = try decoder.sys()
        let fields   = try decoder.contentfulFieldsContainer(keyedBy: FieldKeys.self)
        title        = try fields.decode(String.self, forKey: .title)
        description  = try fields.decode(String.self, forKey: .description)
        
        try fields.resolveLink(forKey: .photo, decoder: decoder) { [weak self] photo in
            self?.photo = photo as? Asset
        }
        try fields.resolveLink(forKey: .chef, decoder: decoder) { [weak self] chef in
            self?.chefName = (chef as? Chef)?.name
        }
        try fields.resolveLinksArray(forKey: .tags, decoder: decoder) { [weak self] array in
            self?.tags = (array as? [Tag])?.map({ tag -> String in
                return tag.name
            }) ?? []
        }
    }
    
    enum FieldKeys: String, CodingKey {
        case title, description, photo, chef, tags
    }

    var recipeDetailsModel: UIRecipeDetailsModel {
        let tagsValues = tags?.joined(separator: ", ") ?? ""
        return UIRecipeDetailsModel(title: title, imagePath: photo?.url, description: description, chefName: chefName ?? "", tags: tagsValues)
    }
}
