//
//  RecipesListViewController.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import UIKit

final class RecipeDetailsViewController: UIViewController {

    // MARK: Outlets
    private let scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.contentInsetAdjustmentBehavior = .never
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alwaysBounceVertical = false
        view.alwaysBounceHorizontal = false
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.automaticallyAdjustsScrollIndicatorInsets = false
        return view
    }()
    
    private let recipeImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .systemBackground
        return imageView
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(recipeImageView)
        stackView.setContentCompressionResistancePriority(.required, for: .vertical)
        return stackView
    }()
    
    private let recipeDetails: UIRecipeDetailsModel
    
    init(_ recipeDetails: UIRecipeDetailsModel) {
        self.recipeDetails = recipeDetails
        super.init(nibName: nil, bundle: nil)
    }
    
    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configureNavigationBar()
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        view.backgroundColor = .systemBackground
        view.addSubview(scrollView)
        scrollView.addSubview(labelsStackView)
        
        NSLayoutConstraint.activate([
            recipeImageView.heightAnchor.constraint(equalTo: view.widthAnchor, constant: -14),
            recipeImageView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -14),
            
            labelsStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            labelsStackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            labelsStackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            labelsStackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            
            labelsStackView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -14),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -14),
            
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 7),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -7),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        if !recipeDetails.title.isEmpty {
            showRecipeTitle(recipeTitle: recipeDetails.title)
        }
        
        if !recipeDetails.chefName.isEmpty {
            showChef(chefName: recipeDetails.chefName)
        }
        
        if !recipeDetails.description.isEmpty {
            showDescription(description: recipeDetails.description)
        }
        
        if !recipeDetails.tags.isEmpty {
            showTags(tags: recipeDetails.tags)
        }
        
        if let url = recipeDetails.imagePath {
            recipeImageView.download(from: url, contentMode: .scaleAspectFit)
        }
        
    }

    private func configureNavigationBar() {
//        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.label]
//        let appearance = UINavigationBarAppearance()
//        appearance.backgroundColor = UIColor.systemBackground
//        appearance.titleTextAttributes = [.foregroundColor: UIColor.label]
//        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.label]
//        appearance.shadowColor = .clear
//        appearance.shadowImage = UIImage()
//
//        navigationController?.navigationBar.tintColor = .green
//        navigationController?.navigationBar.standardAppearance = appearance
//        navigationController?.navigationBar.compactAppearance = appearance
//        navigationController?.navigationBar.scrollEdgeAppearance = appearance
//        navigationController?.hidesBarsOnSwipe = true

    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func showRecipeTitle(recipeTitle: String) {
        let recipeTitleLabel: UILabel = getLabel(for: "Recipe :")
        recipeTitleLabel.tag = 1
        let recipeTitleValueLabel: UILabel = getValueLabel()
        recipeTitleValueLabel.tag = 2
        labelsStackView.addArrangedSubview(recipeTitleLabel)
        labelsStackView.addArrangedSubview(recipeTitleValueLabel)
        recipeTitleValueLabel.text = recipeTitle
    }
    
    private func showChef(chefName: String) {
        
        let chefNameLabel: UILabel = getLabel(for: "Chef :")
        chefNameLabel.tag = 3
        let chefNameValueLabel: UILabel = getValueLabel()
        chefNameValueLabel.tag = 4

        labelsStackView.addArrangedSubview(chefNameLabel)
        labelsStackView.addArrangedSubview(chefNameValueLabel)
        chefNameValueLabel.text = chefName
    }
    
    private func showDescription(description: String) {
        
        let descriptionLabel: UILabel = getLabel(for: "Description :")
        descriptionLabel.tag = 5
        let descriptionValueLabel: UILabel = getValueLabel()
        descriptionValueLabel.tag = 6

        labelsStackView.addArrangedSubview(descriptionLabel)
        labelsStackView.addArrangedSubview(descriptionValueLabel)
        descriptionValueLabel.text = description
    }
    
    private func showTags(tags: String) {
        let tagsLabel: UILabel = getLabel(for: "Tags :")
        tagsLabel.tag = 7
        let tagsValueLabel: UILabel = getValueLabel()
        tagsValueLabel.tag = 8
        labelsStackView.addArrangedSubview(tagsLabel)
        labelsStackView.addArrangedSubview(tagsValueLabel)
        tagsValueLabel.text = tags
        
    }

    // MARK: - Labels Factory

    private func getLabel(for title: String) -> UILabel {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .bold)
        label.textColor = .systemYellow
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = title
        return label
    }
    
    private func getValueLabel() -> UILabel {
        let label = UILabel()
        label.font = .systemFont(ofSize: 13, weight: .regular)
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }
}

struct UIRecipeDetailsModel {
    let title: String
    var imagePath: URL?
    let description: String
    var chefName: String
    var tags: String
}
