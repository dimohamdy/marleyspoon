//
//  RecipesListBuilder.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import UIKit

struct RecipesListBuilder {
    
    static func viewController() -> RecipesListViewController {
        let viewController: RecipesListViewController = RecipesListViewController()
        let router = RecipesListRouter(viewController: viewController)
        let presenter = RecipesListPresenter(output: viewController, router: router)
        viewController.presenter = presenter
        return viewController
    }
}
