//
//  RecipesListViewController.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import UIKit

final class RecipesListViewController: UIViewController {
    
    private(set) var collectionDataSource: RecipesCollectionViewDataSource?
    
    // MARK: Outlets
    private let recipesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 90, height: 90)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(RecipeCollectionCell.self, forCellWithReuseIdentifier: RecipeCollectionCell.identifier)
        collectionView.tag = 1
        collectionView.backgroundColor = .systemBackground
        return collectionView
    }()
    
    var presenter: RecipesListPresenterInput?
    
    // MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configureNavigationBar()
        presenter?.getRecipes()
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        view.backgroundColor = .systemBackground
        view.addSubview(recipesCollectionView)
        NSLayoutConstraint.activate([
            recipesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            recipesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            recipesCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            recipesCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        navigationItem.title = Strings.marleySpoonTitle.localized()

    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.label]
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = UIColor.systemBackground
        appearance.titleTextAttributes = [.foregroundColor: UIColor.label]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.label]
        appearance.shadowColor = .clear
        appearance.shadowImage = UIImage()

        navigationItem.backBarButtonItem?.tintColor = .blue
        navigationController?.navigationBar.tintColor = .label
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        navigationController?.hidesBarsOnSwipe = true
        
    }
}

// MARK: - RecipesListPresenterOutput
extension RecipesListViewController: RecipesListPresenterOutput {
    
    func clearCollection() {
        DispatchQueue.main.async {
            self.collectionDataSource = nil
            self.recipesCollectionView.dataSource = nil
            self.recipesCollectionView.dataSource = nil
            self.recipesCollectionView.reloadData()
        }
    }
    
    func emptyState(emptyPlaceHolderType: EmptyPlaceHolderType) {
        clearCollection()
        recipesCollectionView.setEmptyView(emptyPlaceHolderType: emptyPlaceHolderType, completionBlock: { [weak self] in
            self?.presenter?.getRecipes()
        })
    }
    
    func updateData(error: Error) {
        switch error as? MarleySpoonError {
        case .noResults:
            emptyState(emptyPlaceHolderType: .noResults)
        case .noInternetConnection:
            emptyState(emptyPlaceHolderType: .noInternetConnection)
        default:
            emptyState(emptyPlaceHolderType: .error(message: error.localizedDescription))
        }
    }
    
    func updateData(itemsForCollection: [ItemCollectionViewCellType]) {
        DispatchQueue.main.async {
            //Clear any placeholder view from collectionView
            self.recipesCollectionView.restore()
            
            // Reload the collectionView
            self.collectionDataSource = RecipesCollectionViewDataSource(presenterInput: self.presenter, itemsForCollection: itemsForCollection)
            self.recipesCollectionView.dataSource = self.collectionDataSource
            self.recipesCollectionView.delegate = self.collectionDataSource
            self.recipesCollectionView.reloadData()
            
        }
        
    }
}
