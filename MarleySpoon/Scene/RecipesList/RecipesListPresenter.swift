//
//  RecipesListPresenter.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation

protocol RecipesListPresenterInput: BasePresenterInput {
    func getRecipes()
    func showRecipe(index: Int)
}

protocol RecipesListPresenterOutput: BasePresenterOutput {
    func updateData(error: Error)
    func updateData(itemsForCollection: [ItemCollectionViewCellType])
    func emptyState(emptyPlaceHolderType: EmptyPlaceHolderType)
}

final class RecipesListPresenter {
    
    // MARK: Injections
    private weak var output: RecipesListPresenterOutput?
    private let recipesRepository: WebRecipesRepository
    private let router: RecipesListRoutable

    // internal
    private var recipes: [Recipe] = []

    // MARK: LifeCycle 
    init(output: RecipesListPresenterOutput, router: RecipesListRoutable, recipesRepository: WebRecipesRepository = WebRecipesRepository()) {
        
        self.output = output
        self.router = router
        self.recipesRepository = recipesRepository
        [Notifications.Reachability.connected.name, Notifications.Reachability.notConnected.name].forEach { (notification) in
            NotificationCenter.default.addObserver(self, selector: #selector(changeInternetConnection), name: notification, object: nil)
        }
    }
}

// MARK: - RecipesListPresenterInput
extension RecipesListPresenter: RecipesListPresenterInput {
    func showRecipe(index: Int) {
        let recipe = recipes[index]
        router.show(recipe: recipe.recipeDetailsModel)
    }

    @objc
    private func changeInternetConnection(notification: Notification) {
        if notification.name == Notifications.Reachability.notConnected.name {
            output?.showError(title: Strings.noInternetConnectionTitle.localized(), subtitle: Strings.noInternetConnectionSubtitle.localized())
            output?.updateData(error: MarleySpoonError.noInternetConnection)
        }
    }
}

// MARK: Setup

extension RecipesListPresenter {
    
    func getRecipes() {
        guard Reachability.shared.isConnected else {
            self.output?.updateData(error: MarleySpoonError.noInternetConnection)
            return
        }
        output?.showLoading()

        recipesRepository.recipes { [weak self] result in
            
            guard let self =  self else {
                return
            }
            self.output?.hideLoading()

            switch result {
            case .success(let recipes):

                guard !recipes.isEmpty  else {
                    self.handleNoRecipes()
                    return
                }
                self.handleNewRecipes(recipes: recipes)

            case .failure(let error):
                self.output?.updateData(error: error)
            }
        }
    }
    
    private func handleNewRecipes(recipes: [Recipe]) {
        self.recipes = recipes
        let newItems: [ItemCollectionViewCellType] = createItemsForCollection(recipesArray: recipes)
        output?.updateData(itemsForCollection: newItems)
    }
    
    private func handleNoRecipes() {
        output?.updateData(error: MarleySpoonError.noResults)
    }

    private func createItemsForCollection(recipesArray: [Recipe]) -> [ItemCollectionViewCellType] {
        return recipesArray.map { recipe -> ItemCollectionViewCellType  in
            .recipe(recipe: RecipeCollectionCell.UIRecipeModel(imagePath: recipe.photo?.url, title: recipe.title))
        }
    }
}
