//
//  ItemCollectionViewCellType.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import Foundation

enum ItemCollectionViewCellType {
    case recipe(recipe: RecipeCollectionCell.UIRecipeModel)
}
