//
//  RecipesCollectionViewDataSource.swift
//  MarleySpoon
//
//  Copyright © 2021 BinaryBoy. All rights reserved.
//

import UIKit

final class RecipesCollectionViewDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var itemsForCollection: [ItemCollectionViewCellType] = []
    
    weak var presenterInput: RecipesListPresenterInput?
    
    private struct CellHeightConstant {
        static let heightOfRecipeCell: CGFloat = 120
    }
    
    init(presenterInput: RecipesListPresenterInput?, itemsForCollection: [ItemCollectionViewCellType]) {
        self.itemsForCollection = itemsForCollection
        self.presenterInput = presenterInput
    }
    
    // MARK: - Collection view data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return itemsForCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = itemsForCollection[indexPath.row]
        if case let .recipe(recipe) = item, let cell: RecipeCollectionCell = collectionView.dequeueReusableCell(for: indexPath) {
            cell.configCell(recipe: recipe)
            return cell
        } else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return getRecipeCellSize(collectionView: collectionView)
    }
    
    private func getRecipeCellSize(collectionView: UICollectionView) -> CGSize {
        let widthAndHeight = collectionView.bounds.width / 2.1
        return CGSize(width: widthAndHeight, height: widthAndHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenterInput?.showRecipe(index: indexPath.row)
    }
}
