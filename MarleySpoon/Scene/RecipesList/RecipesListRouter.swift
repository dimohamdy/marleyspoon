//
//  RecipesListRouter.swift
//  MarleySpoon
//
//  Created by BinaryBoy on 7/17/21.
//

import UIKit

protocol RecipesListRoutable {
    func show(recipe: UIRecipeDetailsModel)
}

class RecipesListRouter {

    // MARK: Injections
    weak var viewController: UIViewController?

    // MARK: LifeCycle
    required init(viewController: UIViewController) {
        self.viewController = viewController
    }

}

// MARK: - RecipesListRoutable
extension RecipesListRouter: RecipesListRoutable {
    func show(recipe: UIRecipeDetailsModel) {
        let recipeDetailsViewController = RecipeDetailsViewController(recipe)
        viewController?.navigationController?.pushViewController(recipeDetailsViewController, animated: true)
    }

}
